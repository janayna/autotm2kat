import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

@Keyword
def clickUsingJS(TestObject to, int timeout) {
	WebDriver driver = DriverFactory.getWebDriver()

	WebElement element = WebUiCommonHelper.findWebElement(to, timeout)

	JavascriptExecutor executor = ((driver) as JavascriptExecutor)

	executor.executeScript('arguments[0].click()', element)
}

WebUI.callTestCase(findTestCase('AdmLoginTest'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('AdmPage/Menu/0 menu'))

WebUI.click(findTestObject('AdmPage/Menu/1 configuracoes'))

WebUI.waitForElementClickable(findTestObject('Page_TM2 Digital/a_Atendimento                                    chat'), 
    10)

WebUI.click(findTestObject('Page_TM2 Digital/a_Atendimento                                    chat'))

WebUI.waitForElementVisible(findTestObject('Page_TM2 Digital/a_Grupos de Atendimento                                    people'), 
    10)

WebUI.click(findTestObject('Page_TM2 Digital/a_Grupos de Atendimento                                    people'))

WebUI.waitForPageLoad(200)

WebUI.waitForElementVisible(findTestObject('Page_TM2 Digital/a_add'), 200)

WebUI.clickUsingJS(findTestObject('Page_TM2 Digital/a_add'))

WebUI.verifyTextPresent('Cadastrar Grupo', false)



